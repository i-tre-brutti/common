package test_utils

import (
	"strings"
	"testing"
)

func TestRand(t *testing.T) {
	randString := RandString(10)

	if len(randString) != 10 {
		t.Error("Cannot retrieve a proper random string")
	}
}

func TestRandStringWithCharset(t *testing.T) {
	ret := RandStringWithCharset(5, "abcde")

	if len(ret) != 5 {
		t.Error("Wrong string size")
	}

	if strings.Contains(ret, "r") {
		t.Error("Wrong chars in string")
	}
}
