package test_utils

import "testing"

const CheckMark = "\u2713"
const BallotX = "\u2717"

// CheckTest checks the boolean value of a test and prints the message (success or error)
func CheckTest(t *testing.T, isCorrect bool, message, errExtra string) {
	if isCorrect {
		t.Logf("%s %s", message, CheckMark)
	} else {
		t.Errorf("%s %s %s", message, errExtra, BallotX)
	}
}
