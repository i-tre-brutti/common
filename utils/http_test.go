package utils

import "testing"

func TestAddParamsToURL(t *testing.T) {
	baseURL := "http://www.example.com/asda/qfewf"
	params := []string{"a=1", "b=2"}
	ret := AddParamsToURL(baseURL, params)
	if ret != "http://www.example.com/asda/qfewf?a=1&b=2" {
		t.Errorf("Wrong URL %s", ret)
	}

	baseURL = "http://www.example.com/asda/qfewf?"
	params = []string{"a=1", "b=2"}
	ret = AddParamsToURL(baseURL, params)
	if ret != "http://www.example.com/asda/qfewf?a=1&b=2" {
		t.Errorf("Wrong URL %s", ret)
	}

	baseURL = "http://www.example.com/asda/qfewf?c=3"
	params = []string{"a=1", "b=2"}
	ret = AddParamsToURL(baseURL, params)
	if ret != "http://www.example.com/asda/qfewf?c=3&a=1&b=2" {
		t.Errorf("Wrong URL %s", ret)
	}
}

func TestGetURLParametersSeparator(t *testing.T) {
	r1 := GetURLParametersSeparator("http://www.example.com/asda/qfewf?asdsa")
	r2 := GetURLParametersSeparator("http://www.example.com/asda/qfewf?asdsa&gerger")
	r3 := GetURLParametersSeparator("http://www.example.com/asda/qfewf/")

	if r1 != "&" || r2 != "&" || r3 != "?" {
		t.Error("Wrong parameter separator")
	}
}
