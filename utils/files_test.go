package utils

import "testing"

func TestIsImageFile(t *testing.T) {
	manifest := map[string]bool{
		"file.png":                    true,
		"file.gif":                    true,
		"file.jpg":                    true,
		"file.jpeg":                   true,
		"file.tiff":                   true,
		"file.bmp":                    true,
		"file.svg":                    true,
		"file.txt":                    false,
		"file.json":                   false,
		"file.go":                     false,
		"script":                      false,
		"path/subfolder/file.go":      false,
		"/path/subfolder/file.go":     false,
		"path/subfolder/file.png":     true,
		"/path/subfolder/file.png":    true,
		"/path/subfolder/file.png.go": false,
		"/path/subfolder/file.go.png": true,
		"scriptpng":                   false,
	}
	for k, v := range manifest {
		if IsImageFile(k) != v {
			t.Errorf("Wrong result %v != %v", k, v)
		}
	}
}

func TestIsFontFile(t *testing.T) {
	manifest := map[string]bool{
		"file.otf":                    true,
		"file.gif":                    false,
		"file.ttf":                    true,
		"file.eot":                    true,
		"file.tiff":                   false,
		"file.bmp":                    false,
		"file.svg":                    false,
		"file.txt":                    false,
		"file.json":                   false,
		"file.go":                     false,
		"script":                      false,
		"path/subfolder/file.go":      false,
		"/path/subfolder/file.go":     false,
		"path/subfolder/file.woff":    true,
		"/path/subfolder/file.eot":    true,
		"/path/subfolder/file.png.go": false,
		"/path/subfolder/file.go.png": false,
		"scriptttf":                   false,
	}
	for k, v := range manifest {
		if IsFontFile(k) != v {
			t.Errorf("Wrong result %v != %v", k, v)
		}
	}
}
