package utils

import "testing"

func TestSnakeCaseToCamelCase(t *testing.T) {
	var manifest [][]string
	manifest = append(manifest, []string{"ab_cd_ef", "AbCdEf"})
	manifest = append(manifest, []string{"ab_cd_id", "AbCdID"})
	manifest = append(manifest, []string{"ab_cdid", "AbCdID"})
	manifest = append(manifest, []string{"ab_cd_id_rr", "AbCdIdRr"})

	for _, m := range manifest {
		if SnakeCaseToCamelCase(m[0]) != m[1] {
			t.Errorf("Wrong CamelCase %s => %s", m[0], m[1])
		}
	}
}
