package interfaces

import "time"

// ProjectStatusVersion is the version of the Status document
var ProjectStatusVersion int

func init() {
	ProjectStatusVersion = 1
}

type ProjectStatus struct {
	ProjectID       string               `json:"project_id" bson:"project_id"`
	DocumentVersion int                  `json:"document_version" bson:"document_version"`
	LastRunInfo     LastRunInfoInterface `json:"last_run_info" bson:"last_run_info"`
	Src             SrcData              `json:"src" bson:"src"`
	Issue           IssueData            `json:"issue" bson:"issue"`
}

type LastRunInfoInterface struct {
	Commits   []LastCommitsDesc `json:"commits" bson:"commits"`
	Issues    []LastIssuesDesc  `json:"issues" bson:"issues"`
	Timestamp time.Time         `json:"timestamp" bson:"timestamp"`
}

type LastCommitsDesc struct {
	ProjectID string `json:"project_id" bson:"project_id"`
	CommitID  string `json:"commit_id" bson:"commit_id"`
}

type LastIssuesDesc struct {
	ProjectID string `json:"project_id" bson:"project_id"`
	IssueID   int    `json:"issue_id" bson:"issue_id"`
}

type ProjectStatistics struct {
	ProjectID       string    `json:"project_id" bson:"project_id"`
	CreationDate    time.Time `json:"creation_date" bson:"creation_date"`
	DocumentVersion int       `json:"document_version" bson:"document_version"`
	Age             int64     `json:"age" bson:"age"`
	// Activity is an array of 3 elements
	// cointaining the percentage of the pick
	// dstribution of the developmente life cicle
	Activity  int     `json:"activity" bson:"activity"`
	Expansion float64 `json:"expansion" bson:"expansion"`
	// Trend is a number from 0 to 3 with:
	// 0 = incremental
	// 1 = decremental
	// 2 = stationary
	// 3 = fluctuating
	Trend                 int        `json:"trend" bson:"trend"`
	CodeRelativeDiffusion [5]float64 `json:"code_relative_diffusion" bson:"code_relative_diffusion"`
	CodeAbsoluteDiffusion [5]float64 `json:"code_absolute_diffusion" bson:"code_absolute_diffusion"`
	Popularity            float64    `json:"popularity" bson:"popularity"`
	Quality               float64    `json:"quality" bson:"quality"`
	Contributors          float64    `json:"contributors" bson:"contributors"`
	// AvtivityDistribution is an array of percentages
	// that should sum up to 100% (aka 1.0)
	ActivityDistribution []float64 `json:"activity_distribution" bson:"activity_distribution"`
}

func (v *ProjectStatistics) Clone() *ProjectStatistics {
	var result ProjectStatistics
	result.ProjectID = v.ProjectID
	result.CreationDate = v.CreationDate
	result.DocumentVersion = v.DocumentVersion
	result.Age = v.Age
	result.Activity = v.Activity
	result.Expansion = v.Expansion
	result.Trend = v.Trend
	for i := 0; i < 5; i++ {
		result.CodeRelativeDiffusion[i] = v.CodeRelativeDiffusion[i]
		result.CodeAbsoluteDiffusion[i] = v.CodeAbsoluteDiffusion[i]
	}
	result.Popularity = v.Popularity
	result.Quality = v.Quality
	result.ActivityDistribution = make([]float64, len(v.ActivityDistribution))
	for i := 0; i < len(v.ActivityDistribution); i++ {
		result.ActivityDistribution[i] = v.ActivityDistribution[i]
	}
	return &result
}
