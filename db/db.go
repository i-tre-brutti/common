package db

import (
	"os"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
	mgo "gopkg.in/mgo.v2"
)

var session *mgo.Session
var conf interfaces.GlobalConf

func initGlobalConf() {
	host := "localhost"
	db := "i3brutti"

	if utils.IsTestingEnv() {
		db = db + "_test"
	}

	envHost := os.Getenv("MONGO_HOST")
	if envHost != "" {
		host = envHost
		db = db + "_test"
	}

	conf = interfaces.GlobalConf{
		Host:                 host,
		Db:                   db,
		ConfCollection:       "confs",
		LiveEventsCollection: "live_events",
		StatusCollection:     "statuses",
		StatisticsCollection: "statistics",
	}
}

// InitSession makes the connection to the MongoDB server and saves the session
func InitSession() {
	initGlobalConf()
	s, err := mgo.Dial(conf.Host)
	utils.E(err, 0)
	session = s
}

// CloseSession closes the connection to the db
func CloseSession() {
	session.Close()
}

// GetConfCollection returns the collection used to store project configurations
func GetConfCollection() *mgo.Collection {
	return getDB().C(conf.ConfCollection)
}

// GetStatusCollection returns the collection used to store project statuses
func GetStatusCollection() *mgo.Collection {
	db := getDB()
	return db.C(conf.StatusCollection)
}

// GetStatisticsCollection return the collection used to store project statistics
func GetStatisticsCollection() *mgo.Collection {
	return getDB().C(conf.StatisticsCollection)
}

// GetLiveEventsCollection returns the collection used to store live events
func GetLiveEventsCollection() *mgo.Collection {
	return getDB().C(conf.LiveEventsCollection)
}

func getDB() *mgo.Database {
	if session == nil {
		InitSession()
	}
	return session.DB(conf.Db)
}
