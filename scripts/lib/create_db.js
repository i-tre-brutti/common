db.createCollection("confs");
db.confs.createIndex({"project_id" : 1}, {unique: true, name: "project_id"});

db.createCollection("statuses");
db.statuses.createIndex({"project_id": 1, "document_version": 1}, {unique: true, name: "project_id"});

db.createCollection("statistics");
db.statistics.createIndex({"project_id": 1, "document_version": 1}, {unique: true, name: "project_id"});

db.createCollection("live_events");
db.statistics.createIndex({"project_id": 1}, {unique: false, name: "project_id"});
